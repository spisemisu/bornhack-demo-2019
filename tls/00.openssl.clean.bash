#!/usr/bin/env bash

clear

cas="ca."
uni="bornhack-demo."
moz="cacert.pem"

# Clean before
echo "### Clearing certificate files:"
find . -name  $moz   -delete -print
find . -name "$cas*" -delete -print
find . -name "$uni*" -delete -print
echo
