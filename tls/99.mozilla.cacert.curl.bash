#!/usr/bin/env bash

clear

https://curl.haxx.se/ca/cacert.pem

# Download only if it has changed
curl \
    --remote-name \
    --time-cond \
    cacert.pem \
    https://curl.haxx.se/ca/cacert.pem

## Reference
# - https://curl.haxx.se/docs/caextract.html
