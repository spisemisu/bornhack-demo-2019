#!/usr/bin/env bash

clear

docker build --network=host -t haskell-base-builder --file ./der/Dockerfile.builder .
docker build --network=host -t bornhack-demo        --file ./der/Dockerfile         .
