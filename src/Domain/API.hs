--------------------------------------------------------------------------------

{-# LANGUAGE DeriveDataTypeable #-}

--------------------------------------------------------------------------------

module Domain.API
  ( City
    ( City
    , city
    )
  , Temperature
    ( Temperature
    , temperature
    , timestamp
    )
  ) where

--------------------------------------------------------------------------------

import           Data.Data

--------------------------------------------------------------------------------

data City = City
  { city :: String
  } deriving (Data, Typeable)

data Temperature = Temperature
  { timestamp   :: String
  , temperature :: Double
  } deriving (Data, Typeable)
