--------------------------------------------------------------------------------

{-# LANGUAGE DeriveDataTypeable #-}

--------------------------------------------------------------------------------

module Domain.OSM
  ( Coordinates
    ( lat
    , lon
    )
  ) where

--------------------------------------------------------------------------------

import           Data.Data

--------------------------------------------------------------------------------

data Coordinates = Coordinates
  { lat :: String
  , lon :: String
  } deriving (Data, Typeable)

{- OpenStreetMap Nominatim API

-- HTTP Request (GET):
https://nominatim.openstreetmap.org/search?q=Copenhagen,Denmark&format=json

-- HTTP Response:
[
  {
    "place_id": "108269",
    "licence": "Data © OpenStreetMap contributors, ODbL 1.0. https://osm.org/copyright",
    "osm_type": "node",
    "osm_id": "13707878",
    "boundingbox": [
      "55.5267243",
      "55.8467243",
      "12.4100724",
      "12.7300724"
    ],
    "lat": "55.6867243",
    "lon": "12.5700724",
    "display_name": "Copenhagen, Copenhagen Municipality, Capital Region of Denmark, 1357, Denmark",
    "class": "place",
    "type": "city",
    "importance": 0.9605828604198341,
    "icon": "https://nominatim.openstreetmap.org/images/mapicons/poi_place_city.p.20.png"
  }
]

-}
