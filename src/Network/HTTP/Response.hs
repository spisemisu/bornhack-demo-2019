--------------------------------------------------------------------------------

module Network.HTTP.Response
  ( Response (..)
  , Status (..)
  )
where

--------------------------------------------------------------------------------

import qualified Data.ByteString.Lazy.Char8 as L8

--------------------------------------------------------------------------------

{- HyperText Transfer Protocol (Response message):

The response message consists of the following:

* A status line which includes the status code and reason message (e.g.,
  HTTP/1.1 200 OK, which indicates that the client's request succeeded).

* Response header fields (e.g., Content-Type: text/html).

* An empty line.

* An optional message body.

-}

data Status
  = S200
  | S400
  | S403
  | S404
  | S405
  | NotSupported Int
  deriving Show

data Response
  = Response
    { status  :: Status
    , headers :: [ (String, String) ]
    , body    :: Maybe L8.ByteString
    }
  deriving Show
