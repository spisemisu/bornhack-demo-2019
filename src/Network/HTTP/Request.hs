--------------------------------------------------------------------------------

module Network.HTTP.Request
  ( Method (..)
  , Request (..)
  )
where

--------------------------------------------------------------------------------

import qualified Data.ByteString.Lazy.Char8 as L8

--------------------------------------------------------------------------------

{- HyperText Transfer Protocol (Request message):

The request message consists of the following:

* A request line (e.g., GET /images/logo.png HTTP/1.1, which requests a resource
  called /images/logo.png from the server).

* Request header fields (e.g., Accept-Language: en).

* An empty line.

* An optional message body.

-}

data Method
  = GET
  | POST
  | NotSupported String
  deriving Show

data Request
  = Request
    { method   :: Method
    , resource :: String
    , headers  :: [ (String, String) ]
    , body     :: Maybe L8.ByteString
    }
  deriving Show
