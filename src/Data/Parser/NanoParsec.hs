--------------------------------------------------------------------------------

{-# LANGUAGE FlexibleInstances #-}

--------------------------------------------------------------------------------

module Data.Parser.NanoParsec
  ( Parseable
  , Parser
  , item
  , some, many, sepBy, sepBy1
  , satisfy, oneOf, chainl, chainl1
  , char, string, token, reserved, spaces, eol
  , runParser
  )
where

--------------------------------------------------------------------------------

import           Control.Applicative
  ( Alternative
  )
import           Control.Monad
  ( MonadPlus (mplus, mzero)
  )
import           Control.Monad.Combinators
  ( empty
  , (<|>)
  )
import qualified Data.ByteString            as BS
import qualified Data.ByteString.Lazy.Char8 as L8
import           Data.String
  ( IsString
  )

--------------------------------------------------------------------------------

newtype Parser s a = Parser { parse :: s -> [ (a, s) ] }

class (Eq a, IsString a) => Parseable a where
  nil :: a -> Bool
  hd  :: a -> Char
  tl  :: a -> a

instance Parseable String where
  nil = (== [])
  hd  =  head
  tl  =  tail

instance Parseable BS.ByteString where
  nil =                         BS.null
  hd  = toEnum . fromIntegral . BS.head
  tl  =                         BS.tail

instance Parseable L8.ByteString where
  nil =                         L8.null
  hd  =                         L8.head
  tl  =                         L8.tail

--------------------------------------------------------------------------------

instance (Parseable s) => Functor (Parser s) where
  fmap f (Parser cs) =
    Parser $ \s -> [(f a, b) | (a, b) <- cs s]

instance (Parseable s) => Applicative (Parser s) where
  pure = return
  (Parser cs1) <*> (Parser cs2) =
    Parser $ \s -> [(f a, s2) | (f, s1) <- cs1 s, (a, s2) <- cs2 s1]

instance (Parseable s) => Monad (Parser s) where
  return = unit
  (>>=)  = bind

instance (Parseable s) => MonadPlus (Parser s) where
  mzero = failure
  mplus = combine

instance (Parseable s) => Alternative (Parser s) where
  empty = mzero
  (<|>) = option

--------------------------------------------------------------------------------

bind
  :: Parser s a
  -> (a -> Parser s b)
  -> Parser s b
bind p f =
  Parser $ \s -> concatMap (\(a, s') -> parse (f a) s') $ parse p s

unit
  :: a
  -> Parser s a
unit a =
  Parser $ \s -> [ (a, s) ]

combine
  :: Parser s a
  -> Parser s a
  -> Parser s a
combine p q =
  Parser $ \s -> parse p s ++ parse q s

failure
  :: Parser s a
failure =
  Parser $ \_ -> []

option
  :: Parser s a
  -> Parser s a
  -> Parser s a
option p q =
  Parser
  $ \s ->
      case parse p s of
        [ ] -> parse q s
        res -> res

--------------------------------------------------------------------------------

item
  :: (Parseable s)
  => Parser s Char
item =
  Parser
  $ \s ->
      case nil s of
        True  -> []
        False -> [ (hd s, tl s) ]

--------------------------------------------------------------------------------

-- | One or more.
some
  :: (Alternative f)
  => f a
  -> f [a]
some v = some_v
  where
    many_v = some_v <|> pure []
    some_v = (:) <$> v <*> many_v

-- | Zero or more.
many
  :: (Alternative f)
  => f a
  -> f [a]
many v = many_v
  where
    many_v = some_v <|> pure []
    some_v = (:) <$> v <*> many_v

-- | One or more.
sepBy1
  :: (Alternative f)
  => f a
  -> f b
  -> f [a]
sepBy1 p sep =
  (:) <$> p <*> (many $ sep *> p)

-- | Zero or more.
sepBy
  :: (Alternative f)
  => f a
  -> f b
  -> f [a]
sepBy p sep =
  sepBy1 p sep <|> pure []

--------------------------------------------------------------------------------

satisfy
  :: (Parseable s)
  => (Char -> Bool)
  -> Parser s Char
satisfy p =
  item `bind`
  \c ->
    if p c
    then unit c
    else Parser $ \_ -> []

--------------------------------------------------------------------------------

oneOf
  :: (Parseable s)
  => [Char]
  -> Parser s Char
oneOf s =
  -- satisfy $ flip elem s
  satisfy (`elem` s)

chainl
  :: (Parseable s)
  => Parser s a
  -> Parser s (a -> a -> a)
  -> a
  -> Parser s a
chainl p op a =
  (p `chainl1` op) <|> return a

chainl1
  :: (Parseable s)
  => Parser s a
  -> Parser s (a -> a -> a)
  -> Parser s a
p `chainl1` op =
  do {a <- p; rest a}
  where
    rest a =
      (do f <- op
          b <- p
          rest (f a b)) <|> return a

--------------------------------------------------------------------------------

char
  :: (Parseable s)
  => Char
  -> Parser s Char
char c = satisfy (c ==)

string
  :: (Parseable s)
  => String
  -> Parser s String
string []     = return []
string (c:cs) = (:) <$> char c <*> string cs
  --do { _ <- char c; _ <- string cs; return (c:cs)}

token
  :: (Parseable s)
  => Parser s a
  -> Parser s a
token p = aux <$> p <*> spaces
  where
    aux a _ = a
    --do { a <- p; _ <- spaces ; return a}

reserved
  :: (Parseable s)
  => String
  -> Parser s String
reserved s = token $ string s

spaces
  :: (Parseable s)
  => Parser s String
spaces = many $ char ' '
  -- oneOf $ oneOf " \n\r"

eol
  :: (Parseable s)
  => Parser s String
eol = reserved "\n\r"

--------------------------------------------------------------------------------

runParser
  :: (Parseable s)
  => Parser s a
  -> s
  -> Either String a
runParser m s =
  ps $ parse m s
  where
    ps [   ] = Left "Parser error."
    ps (x:_) = aux x
    aux x
      |       nil $ rest = Right $ fst $ x
      | not . nil $ rest = Left  $ "Parser didn't consume entire stream."
      | otherwise        = Left  $ "Parser error."
      where
        rest = snd x
