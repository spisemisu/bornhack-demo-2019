--------------------------------------------------------------------------------

{-# LANGUAGE OverloadedStrings #-}

--------------------------------------------------------------------------------

module Data.Parser.HTTP.Response
  ( parse
  )
where

--------------------------------------------------------------------------------

import qualified Data.ByteString.Lazy.Char8 as L8
import           Data.Char
  ( isDigit
  )

import           Data.Parser.HTTP.Common
  ( findSubByteString
  )
import           Data.Parser.NanoParsec
import qualified Network.HTTP.Response      as HTTP

--------------------------------------------------------------------------------

noeol
  :: (Parseable s)
  => Parser s Char

status'
  :: (Parseable s)
  => Parser s HTTP.Status

headers'
  :: (Parseable s)
  => Parser s [ (String, String) ]

response
  :: (Parseable s)
  => L8.ByteString
  -> Parser s HTTP.Response

chunked ::
  HTTP.Response
  -> Either String HTTP.Response

parse
  :: L8.ByteString
  -> Either String HTTP.Response

--------------------------------------------------------------------------------

noeol =
  satisfy $ \c -> '\r' /= c && '\n' /= c

status' =
  do
    _ <-        reserved "HTTP/1.1"
    _ <-        spaces
    s <- some $ satisfy  isDigit
    _ <- many   noeol
    _ <-        reserved "\r\n"
    return $ aux $ read $ s
    where
      aux 200 = HTTP.S200
      aux 400 = HTTP.S400
      aux 403 = HTTP.S403
      aux 404 = HTTP.S404
      aux 405 = HTTP.S405
      aux  n  = HTTP.NotSupported n

headers' =
  do
    h <- pair `sepBy` reserved "\r\n"
    return $ h
    where
      nono = satisfy $ \c -> ':' /= c && '\r' /= c && '\n' /= c
      pair =
        do
          key <- some nono
          ___ <- reserved ": "
          val <- some $ noeol
          return $ (key, val)

response b =
  do
    s <- status'
    h <- headers'
    _ <- reserved "\r\n"
    return
      $ HTTP.Response s h
      $ if b == L8.empty then Nothing else Just b

chunked hr@(HTTP.Response status headers body) =
  case any ((==) ("Transfer-Encoding", "chunked")) headers of
    False -> Right hr
    True  ->
      case body of
        Nothing -> Right hr
        Just bs ->
          Right
          $ HTTP.Response status headers
          $ Just $ L8.unlines $ aux 2 $ drop 1 $ L8.lines bs
  where
    aux _ [] = []
    aux n xs = take (n-1) xs ++ aux n (drop n xs)

parse bs =
  aux
  where
    aux =
      case findSubByteString "\r\n\r\n" bs of
        Nothing -> Left "Not a valid HTTP response."
        Just  n ->
          runParser (response b) h >>= chunked
          where
            h = L8.take (i+2) bs
            b = L8.drop (i+4) bs
            i = fromIntegral n
