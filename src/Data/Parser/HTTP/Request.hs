--------------------------------------------------------------------------------

{-# LANGUAGE OverloadedStrings #-}

--------------------------------------------------------------------------------

module Data.Parser.HTTP.Request
  ( parse
  )
where

--------------------------------------------------------------------------------

import qualified Data.ByteString.Lazy.Char8 as L8

import           Data.Parser.HTTP.Common
  ( findSubByteString
  )
import           Data.Parser.NanoParsec
import qualified Network.HTTP.Request       as HTTP

--------------------------------------------------------------------------------

noeol
  :: (Parseable s)
  => Parser s Char

reqline
  :: (Parseable s)
  => Parser s (HTTP.Method, String)

headers'
  :: (Parseable s)
  => Parser s [ (String, String) ]

request
  :: (Parseable s)
  => L8.ByteString
  -> Parser s HTTP.Request

chunked ::
  HTTP.Request
  -> Either String HTTP.Request

parse
  :: L8.ByteString
  -> Either String HTTP.Request

--------------------------------------------------------------------------------

noeol =
  satisfy $ \c -> '\r' /= c && '\n' /= c

reqline =
  do
    m <- some $ satisfy (/= ' ')
    _ <- spaces
    r <- some $ satisfy (/= ' ')
    _ <- spaces
    _ <- reserved "HTTP/1.1"
    _ <- reserved "\r\n"
    return (aux m, r)
    where
      aux "GET"  = HTTP.GET
      aux "POST" = HTTP.POST
      aux  name  = HTTP.NotSupported name

headers' =
  do
    h <- pair `sepBy` reserved "\r\n"
    return $ h
    where
      nono = satisfy $ \c -> ':' /= c && '\r' /= c && '\n' /= c
      pair =
        do
          key <- some nono
          ___ <- reserved ": "
          val <- some $ noeol
          return $ (key, val)

request b =
  do
    l <- reqline
    h <- headers'
    _ <- reserved "\r\n"
    return
      $ HTTP.Request (fst l) (snd l) h
      $ if b == L8.empty then Nothing else Just b

chunked hr@(HTTP.Request method resource headers body) =
  case any ((==) ("Transfer-Encoding", "chunked")) headers of
    False -> Right hr
    True  ->
      case body of
        Nothing -> Right hr
        Just bs ->
          Right
          $ HTTP.Request method resource headers
          $ Just $ L8.unlines $ aux 2 $ drop 1 $ L8.lines bs
  where
    aux _ [] = []
    aux n xs = take (n-1) xs ++ aux n (drop n xs)

parse bs =
  aux
  where
    aux =
      case findSubByteString "\r\n\r\n" bs of
        Nothing -> Left "Not a valid HTTP request."
        Just  n ->
          runParser (request b) h >>= chunked
          where
            h = L8.take (i+2) bs
            b = L8.drop (i+4) bs
            i = fromIntegral n
