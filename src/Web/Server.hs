--------------------------------------------------------------------------------

{-# LANGUAGE OverloadedStrings #-}

--------------------------------------------------------------------------------

module Web.Server
  ( server
  ) where

--------------------------------------------------------------------------------

import           Control.Concurrent
  ( forkFinally
  )
import qualified Data.ByteString.Char8 as C8
import           Data.Default.Class
  ( def
  )
import           Data.Maybe
  ( catMaybes
  , listToMaybe
  )

import           Network.Socket        hiding
  ( recv
  , send
  )
import qualified Network.TLS           as TLS
import qualified Network.TLS.Extra     as TLSX

import qualified Data.X509             as X509

--------------------------------------------------------------------------------

spawn
  :: (Socket, SockAddr)
  -> (TLS.Context -> IO ())
  -> TLS.Credentials
  -> IO ()

loop
  :: Socket
  -> (TLS.Context -> IO ())
  -> Either String TLS.Credential
  -> IO ()

server
  :: Word
  -> String
  -> (TLS.Context -> IO ())
  -> IO ()

--------------------------------------------------------------------------------

spawn (sock, _) app creds =
  do
    ctx <- TLS.contextNew sock $ para creds
    ___ <- TLS.handshake  ctx
    app ctx
  where
    para x509 =
      def
      { TLS.serverWantClientCert = False
      , TLS.serverShared         =
        def
        { TLS.sharedCredentials  = x509
        }
      , TLS.serverSupported      =
        def
        { TLS.supportedCiphers  = TLSX.ciphersuite_strong
        , TLS.supportedVersions = [ TLS.TLS12 ]
        }
      }

loop sock app x509@(Right creds) =
  do
    conn <- accept sock
    ____ <- putStrLn $ ("+ Connected to: " ++) $ (show . snd) conn
    ____ <-
      forkFinally
      (spawn conn app $ TLS.Credentials [creds])
      (
        \ _ ->
          do
            close $ fst $ conn
            putStrLn $ ("- Disconnected: " ++) $ (show . snd) conn
      )
    loop sock app x509
loop _ _ (Left msg) =
  putStrLn $ msg

server port path app =
  do
    x509 <-
      TLS.credentialLoadX509
      (path ++ "../tls/bornhack-demo.public.crt")
      (path ++ "../tls/bornhack-demo.secret.key")

    host <- dncn x509
    addr <- head <$> getAddrInfo (Just hint) host (Just $ show port)
    sock <- socket (addrFamily addr) Stream defaultProtocol
    ____ <- setSocketOption sock ReuseAddr 1
    ____ <- setSocketOption sock NoDelay   1
    ____ <- bind sock $ addrAddress addr
    ____ <- info host
    ____ <- listen sock maxListenQueue
    loop sock app x509
      where
        hint  = defaultHints { addrSocketType = Stream }
        info (Nothing) = pure ()
        info (Just  h) = putStrLn $ "Listening on: " ++ h ++ ":" ++ show port
        dncn (Left  ____________________________) = pure Nothing
        dncn (Right (X509.CertificateChain cc,_)) =
          pure . listToMaybe . catMaybes
          $
          (
            Just . C8.unpack . X509.getCharacterStringRawData =<<
          )
          <$>
          (
            X509.getDnElement X509.DnCommonName .
            X509.certSubjectDN .
            X509.getCertificate
          )
          <$> cc
