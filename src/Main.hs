--------------------------------------------------------------------------------

module Main (main) where

--------------------------------------------------------------------------------

import qualified Effects.Granulated         as EFF

--------------------------------------------------------------------------------

import qualified Data.ByteString.Lazy.Char8 as L8
import qualified Network.TLS                as TLS

--------------------------------------------------------------------------------

import qualified Network.HTTP.Request       as HTTP
import qualified Network.HTTP.Response      as HTTP

import qualified Data.JSON                  as JSON
import qualified Data.Parser.HTTP.Request   as REQ
import qualified Data.Parser.HTTP.Response  as RESP

import qualified Domain.API                 as API
import qualified Domain.MET                 as MET
import qualified Domain.OSM                 as OSM

--------------------------------------------------------------------------------

main :: IO ()
main =
  granulated

--------------------------------------------------------------------------------

granulated
  ::
    ( EFF.EnvM    io
    , EFF.ServerM io
    , EFF.TlsM    io
    , EFF.OsmM    io
    , EFF.TimeM   io
    , EFF.MetM    io
    )
  => io ()
granulated =
  EFF.port >>= \ tcp ->
  EFF.path >>= \ bin ->
  EFF.start tcp bin app

--------------------------------------------------------------------------------

app
  :: ( EFF.TlsM  io
     , EFF.OsmM  io
     , EFF.TimeM io
     , EFF.MetM  io
     )
  => TLS.Context
  -> io ()
app ctx =
  EFF.pull ctx    >>= \ input        ->
  incoming' input >>= \ (Right city) ->
  osm city        >>= \ (Right cord) ->
  EFF.iso8601     >>= \        time  ->
  met time cord   >>= \ (Right temp) ->
  outgoing' temp  >>= \ output       ->
  EFF.push ctx output
  where
    incoming' = pure . incoming
    outgoing' = pure . outgoing
    --time = "1970-01-01T00:00:00Z"

--------------------------------------------------------------------------------

incoming :: L8.ByteString -> Either String API.City
incoming =
  Right . aux
  where
    aux bs =
      JSON.decodeJSON . L8.unpack $ body
      where
        (Right (HTTP.Request _ _ _ (Just body))) = REQ.parse bs

osm
  :: (EFF.OsmM io)
  => API.City
  -> io (Either String OSM.Coordinates)
osm c =
  aux <$> (EFF.osm $ (qry . API.city) c)
  where
    aux (Left  er) = Left er
    aux (Right bs) =
      (
        case coords of
          [ ] -> Left  "No lat/lon coordinates for the provided city."
          x:_ -> Right x
      )
      where
        (Right (HTTP.Response _ _ (Just body))) = RESP.parse bs
        coords = (JSON.decodeJSON . L8.unpack) body
    qry x = Just $ ("/search?q=" ++ x ++ "&format=json")

met
  :: (EFF.MetM io)
  => String
  -> OSM.Coordinates
  -> io (Either String API.Temperature)
met ts cs =
  aux <$> (EFF.met $ qry lat lon)
  where
    lat = OSM.lat cs
    lon = OSM.lon cs
    r2e x =
      case x of
        JSON.Ok    o -> Right o
        JSON.Error e -> Left  e
    aux (Left  er) = Left er
    aux (Right bs) =
      (
        r2e $ (JSON.decode . L8.unpack) body
      ) >>=
      Right .
      API.Temperature ts .
      MET.value .
      MET.temperature .
      MET.location .
      MET.time .
      MET.product
      where
        (Right (HTTP.Response _ _ (Just body))) = RESP.parse bs
    qry x y =
      Just $ "/weatherapi/locationforecast/1.9/.json?lat=" ++ x ++ "&lon=" ++ y

outgoing :: API.Temperature -> L8.ByteString
outgoing = L8.pack . JSON.encodeJSON

{- Note: Moving Effect context doesn't impact reproducible build (semantics) -}
