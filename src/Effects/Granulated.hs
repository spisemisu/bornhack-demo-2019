--------------------------------------------------------------------------------

module Effects.Granulated
  ( EnvM
    ( port
    , path
    )
  , TlsM
    ( pull
    , push
    )
  , TimeM
    ( iso8601
    )
  , ServerM
    ( start
    )
  , OsmM
    ( osm
    )
  , MetM
    ( met
    )
  ) where

--------------------------------------------------------------------------------

import qualified Data.Maybe                 as MAY
import qualified System.Environment         as ENV

import qualified Data.ByteString.Lazy.Char8 as L8
import qualified Network.TLS                as TLS

import qualified Data.Time.Clock            as TIME
import qualified Data.Time.Format           as TIME

--------------------------------------------------------------------------------

import qualified Web.Client                 as WEB
import qualified Web.Server                 as WEB

--------------------------------------------------------------------------------

class (Monad m) => EnvM m where
  port :: m Word
  path :: m String

class (Monad m) => TlsM m where
  pull :: TLS.Context -> m L8.ByteString
  push :: TLS.Context ->   L8.ByteString -> m ()

class (Monad m) => TimeM m where
  iso8601 :: m String

class (Monad m) => ServerM m where
  start
    :: Word
    -> String
    -> (TLS.Context -> m ())
    -> m ()

class (Monad m) => OsmM m where
  osm
    :: Maybe String
    -> m (Either String L8.ByteString)

class (Monad m) => MetM m where
  met
    :: Maybe String
    -> m (Either String L8.ByteString)

--------------------------------------------------------------------------------

instance EnvM IO where
  port =
    MAY.fromMaybe 8443 . MAY.listToMaybe . (map read) <$> ENV.getArgs
  path =
    ENV.getExecutablePath >>= \ p -> pure $ take (length p - n) p
    where
      n = length "bornhack-demo"

instance TlsM IO where
  pull ctx =
    L8.fromChunks . (:[]) <$> TLS.recvData ctx
  push =
    TLS.sendData

instance TimeM IO where
  iso8601 =
    (TIME.formatTime TIME.defaultTimeLocale fm) <$> TIME.getCurrentTime
    where
      fm = "%FT%T%0QZ"

instance ServerM IO where
  start =
    WEB.server

instance OsmM IO where
  osm =
    WEB.get url tls
    where
      url = "nominatim.openstreetmap.org"
      tls = Nothing -- Equivalent to: Just $ 443

instance MetM IO where
  met =
    WEB.get url tls
    where
      url = "api.met.no"
      tls = Nothing -- Equivalent to: Just $ 443
